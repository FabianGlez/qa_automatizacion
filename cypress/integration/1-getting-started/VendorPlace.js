/// <reference types="Cypress" />

context('Startup', () => {
  beforeEach(() => {
    cy.visit('/es/login');
  });

  it('Funcion para realizar el login a la aplicación', () => {

    // Insertamos los datos del usuario 
    cy.get('input[name="email"]')
      .type('automation@test.com')
      .should('have.value', 'automation@test.com');

    // Insertamos la contraseña del usuario
    cy.get('input[name="password"]')
      .type('Abc12345.')
      .should('have.value', 'Abc12345.');

    // Damos clic en el boton continuar
    cy.get('.MuiButton-label').click();

    // Seteamos el contextRoot "/es/dashboard" a 1 segundos
    cy.location('pathname', { timeout: 10000 }).should('eq', '/es/dashboard');

    // Navegamos a la funcion cuentas por pagar
    cy.get('.MuiList-root > :nth-child(3) > .MuiBox-root').click();

    // Setemos el contextRoot "/es/account-payables/accounts/mailbox"
    cy.location('pathname', { timeout: 10000 }).should('eq', '/es/account-payables/accounts/mailbox');

    // Damos de alta una nueva cuenta por pagar
    cy.get('.MuiButton-label').click();
    cy.get('input[aria-autocomplete="list"]')
      .type('Happy Path')
      .should('have.value', 'Happy Path');

    cy.get('.MuiAutocomplete-option').click();
    cy.get('.jss1385 > .MuiButton-label').click();
    cy.get('.jss1364 > input').clear().type('60').should('have.value', '60');

    cy.get('.jss2308 > .MuiFormControl-root > .MuiInputBase-root > .MuiInputBase-input')
      .clear()
      .type('FSDGXDZBDZFHFHDFSHDF%&(/&()/&)%&)&/(&/(%(BSDFBCXXBXBFDY&%$&HSDFHFD&$%$/%$/%&/&%/%"#')
      .should('have.value', 'FSDGXDZBDZFHFHDFSHDF%&(/&()/&)%&)&/(&/(%(BSDFBCXXBXBFDY&%$&HSDFHFD&$%$/%$/%&/&%/%"#');

    cy.get('.jss1385 > .MuiButton-label').click();

    cy.get('.jss4808 > .MuiButtonBase-root > .MuiIconButton-label > .MuiSvgIcon-root').click();

    cy.get('[style="position: fixed; z-index: 1300; inset: 0px;"] > .MuiPaper-root > .MuiList-root > :nth-child(3)').click();
      //.type('Ver detalle').click();
      //.should('have.value', 'Ver detalle');

      cy.get('.jss5337 > .MuiBox-root > .MuiButton-outlined > .MuiButton-label').click();

      cy.get('.MuiButton-textSecondary').click();

  });

});